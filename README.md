# Mirrorman

Mirrorman is a mirror server written to serve [pacman](https://www.archlinux.org/pacman/)-based packages. It's written in PHP7, built upon the [Mako PHP framework](https://makoframework.com/).

## Quickstart

1. Clone this repository, either into an existing webserver (configured to run PHP) of your choice, or use it as a standalone.

2. Choose one or several mirror servers for [Arch](https://www.archlinux.org/mirrors/), [Manjaro](https://repo.manjaro.org/), or any other Arch-based mirror distro and add them to `config/mirrors.php`, for example:

```
<?php

return
[
	'archlinux' =>
	[
		'url' => '%s/%s/os/%s/%s',
		'servers' =>
		[
			'https://mirror.neuf.no/archlinux/',
			'https://archlinux.dynamict.se/$repo/os/$arch',
			'https://archlinux.dynamict.se/$repo/os/$arch',
		],
		'repositories' =>
		[
			'core',
			'extra',
			'community',
			'multilib',
		],
	],
	'manjaro' =>
	[
		'url' => '%s/%s/%s/%s',
		'servers' =>
		[
			'http://mirror.terrahost.no/linux/manjaro/stable/',
			'https://mirror.zetup.net/manjaro/stable/',
			'https://ftp.lysator.liu.se/pub/manjaro/stable/',
		],
		'repositories' =>
		[
			'core',
			'extra',
			'community',
			'multilib',
		],
	],
];

```

3. Set your server as your preferred mirror on your machine in `/etc/pacman.d/mirrorlist` (or wherever your mirrorlist file is):

```
Server = http://localhost/mirrorman/archlinux/$repo/os/$arch
```

4. ??? (`pacman -Syyu`)

5. Profit.

## Licensing

Mirrorman is licensed under the GPLv3.

The Mako PHP framework is licensed under BSD-3-clause.


