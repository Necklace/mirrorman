<?php

// This file gets included at the end of the application boot sequence

// -------------------------------------------------------------
// Assign global view variables
// -------------------------------------------------------------

$container->get('view')
->assign('url', $container->get('urlBuilder'));
