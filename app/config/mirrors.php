<?php

return
[
	'archlinux' =>
	[
		'url' => '%s/%s/os/%s/%s',
		'timeout' => 1,
		'servers' =>
		[
			'https://mirror.neuf.no/archlinux/',
			'https://archlinux.dynamict.se/',
			'https://archlinux.dynamict.se/',
		],
		'repositories' =>
		[
			'core',
			'extra',
			'community',
			'multilib',
		],
	],
	'manjaro' =>
	[
		'url' => '%s/%s/%s/%s',
		'timeout' => 1,
		'servers' =>
		[
			'https://mirror.zetup.net/manjaro/stable/',
			'https://ftp.lysator.liu.se/pub/manjaro/stable/',
		],
		'repositories' =>
		[
			'core',
			'extra',
			'community',
			'multilib',
		],
	],
	'msys2' =>
	[
		'url' => '%s/%s/%s/%s',
		'timeout' => 1,
		'servers' =>
		[
			'http://repo.msys2.org',
		],
		'repositories' =>
		[
			'mingw',
			'msys',
		],
	],
	'parabola' =>
	[
		'url' => '%s/%s/os/%s/%s',
		'timeout' => 1,
		'servers' =>
		[
			'https://ftp.acc.umu.se/mirror/parabola.nu/',
			'https://mirrors.dotsrc.org/parabola/',
			'https://mirror.fsf.org/parabola/',
		],
	],
	'hyperbola' =>
	[
		'url' => '%s/%s/os/%s/%s',
		'timeout' => 1,
		'servers' =>
		[
			'https://ftp.nluug.nl/os/Linux/distr/hyperbola/gnu-plus-linux-libre/stable/',
			'https://mirror.fsf.org/hyperbola/gnu-plus-linux-libre/stable/',
		],
	],
];
