<?php

namespace app\controllers;

use app\models\Package;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Stream\Stream;
use mako\http\exceptions\HttpException;
use mako\http\exceptions\NotFoundException;
use mako\http\response\senders\Stream as StreamSender;
use mako\http\routing\Controller;
use mako\view\ViewFactory;
use Necklace\XxHash\Xxh3\XxHash128;

/**
 * Mirror controller.
 */
class Mirror extends Controller
{
	/**
	 * Get config timeout for distro.
	 *
	 * @param  string $distro Distro
	 * @return float
	 */
	private function getTimeout($distro): float
	{
		return $this->config->get('mirrors.' . $distro . '.timeout') ? (float) $this->config->get('mirrors.' . $distro . '.url') : (float) 5;
	}

	/**
	 * This should probably be a mako function..
	 *
	 * @param  string $dir
	 * @return int
	 */
	private function folderSize($dir): int
	{
		$size = 0;
		$glob = $this->fileSystem->glob(rtrim($dir, '/') . '/*', GLOB_NOSORT);

		if($glob)
		{
			foreach($glob as $each)
			{
				$size += $this->fileSystem->isFile($each) ? $this->fileSystem->size($each) : $this->folderSize($each);
			}
		}

		return $size;
	}

	/**
	 * Count amount of saveable data (duplicate files using a hashmap).
	 *
	 * @param  string $dir
	 * @return int
	 */
	private function saveAble($dir): int
	{
		$size = 0;
		$glob = $this->fileSystem->glob(rtrim($dir, '/') . '/*', GLOB_NOSORT);
		$xxh = new XxHash128;

		if($glob)
		{
			$hashmap = [];

			foreach($glob as $each)
			{
				$hash = $xxh->hashFile($each);

				if(!in_array($hash, $hashmap))
				{
					array_push($hashmap, $hash);
				}
				else
				{
					$size += $this->fileSystem->isFile($each) ? $this->fileSystem->size($each) : $this->folderSize($each);
				}
			}
		}

		return $size;
	}

	/**
	 * Welcome route.
	 *
	 * @param  \mako\view\ViewFactory $view View factory
	 * @return string
	 */
	public function welcome(ViewFactory $view): string
	{
		$packages = sprintf('%s/storage/packages/', $this->app->getPath());

		return $view->render('welcome',
		[
			'size'     => $this->humanizer->fileSize($this->folderSize($packages)),
			'saveable' => 0, // $this->humanizer->fileSize($this->saveAble($packages)),
		]);
	}

	/**
	 * Remove all files with the same basename (aka all versions of the same package).
	 *
	 * @param  string $filepath
	 * @param  string $filename
	 * @return void
	 */
	private function removeAll($filepath, $filename): void
	{
		$base = (string) preg_replace('/-\d.*/', '', $filepath);

		$removables = $this->fileSystem->glob($base . '*');

		if($removables)
		{
			foreach($removables as $removeable)
			{
				if(!strpos($removeable, $filename))
				{
					$this->fileSystem->remove($removeable);
				}
			}
		}
	}

	/**
	 * Parse Range request to check if it's a partial request or not.
	 *
	 * @param  string     $range    Range header
	 * @param  int        $fileSize Filesize
	 * @return bool|array False or Matches
	 */
	private function checkPartial($range, $fileSize)
	{
		// Check if multipart

		if(strpos($range, ','))
		{
			return false;
		}

		preg_match('/bytes\=(\d+)-(\d+)?/', $range, $matches);

		switch(count($matches))
		{
			case 2:
				$matches[2] = $fileSize - 1;
				break;
			case 3:
				break;
			default:
				return false;
		}

		$length = $matches[2] - $matches[1] + 1;

		if($length > $fileSize)
		{
			return false;
		}

		return ['start' => $matches[1], 'end' => $matches[2], 'length' => $length];
	}

	/**
	 * Downloads, streams, and saves a file at the same time.
	 *
	 * @param  string                             $url
	 * @param  string                             $distro
	 * @param  string                             $filepath
	 * @param  string                             $filename
	 * @return \mako\http\response\senders\Stream
	 */
	private function downloadAndStream($url, $distro, $filepath, $filename): StreamSender
	{
		// Delete previous versions of the file

		$this->removeAll($filepath, $filename);
		$response = (new Client)->request('GET', $url, ['stream' => true, 'timeout' => $this->getTimeout($distro), 'connect_timeout' => $this->getTimeout($distro)]);

		$body = $response->getBody();

		if($this->request->getHeaders()->get('Range'))
		{
			// How the frick did you think this would work? Server needs to download entire file either way. Sigh.

			$this->response->setStatus(416);
			return $this->streamResponse(function($stream): void {
				// Wow look, it's nothing
			});
		}

		$this->response->getHeaders()
			->add('Accept-Ranges', 'bytes')
			->add('Content-Disposition', sprintf('attachment; filename=%s', $filename))
			->add('Content-Length', $response->getHeader('Content-Length')[0]);

		return $this->streamResponse(function($stream) use ($body, $filepath): void
		{
			// Make sure current file doesn't exist, so we can download it (just in case)

			if($this->fileSystem->has($filepath))
			{
				$this->fileSystem->remove($filepath);
			}

			$file = $this->fileSystem->file($filepath, 'w+');

			while(!$body->eof())
			{
				$chunk = $body->read(4096);

				$stream->flush($chunk);

				$file->fwrite($chunk);
			}
		});

	}

	/**
	 * Streams an already downloaded file.
	 *
	 * @param  string                             $filepath
	 * @param  string                             $filename
	 * @return \mako\http\response\senders\Stream
	 */
	private function streamFile($filepath, $filename): StreamSender
	{
		$file = $this->fileSystem->file($filepath);

		$range = $this->request->getHeaders()->get('Range');

		$partial = false;

		if($range)
		{
			$partial = $this->checkPartial($range, $file->getSize());

			if(!$partial)
			{
				$this->response->setStatus(416);
				return $this->streamResponse(function($stream): void {
					// Wow look, it's nothing
				});
			}

			$file->fseek($partial['start']);

			$this->response->setStatus(206);
			$this->response->getHeaders()
				->add('Accept-Ranges', 'bytes')
				->add('Content-Range', sprintf('bytes %s-%s/%s', $partial['start'], $partial['end'], $file->getSize()))
				->add('Content-Disposition', sprintf('attachment; filename=%s', $filename))
				->add('Content-Length', (string) $partial['length']);
		}
		else
		{
			$this->response->getHeaders()
				->add('Accept-Ranges', 'bytes')
				->add('Content-Disposition', sprintf('attachment; filename=%s', $filename))
				->add('Content-Length', (string) $file->getSize());
		}

		return $this->streamResponse(function($stream) use ($file, $partial): void
		{
			if(!$partial)
			{
				while(!$file->eof())
				{
					$stream->flush($file->fread(4096));
				}
			}
			else
			{
				$count = 0;
				$offset = $partial['length'] % 4096;

				while($count < floor($partial['length']/4096))
				{
					$stream->flush($file->fread(4096));
					$count++;
				}

				$stream->flush($file->fread($offset));
			}
		});
	}

	/**
	 * Get route.
	 *
	 * @param  string                             $distro
	 * @param  string                             $repo
	 * @param  string                             $arch
	 * @param  string                             $filename
	 * @return \mako\http\response\senders\Stream
	 */
	public function get($distro, $repo, $arch, $filename): StreamSender
	{
		// Go through all the configured mirror servers

		foreach($this->config->get('mirrors.' . $distro . '.servers') as $server)
		{
			// Build url for the requested file

			$url = sprintf($this->config->get('mirrors.' . $distro . '.url'), rtrim($server, '/'), $repo, $arch, $filename);

			$filepath = sprintf('%s/storage/packages/%s_%s_%s_%s', $this->app->getPath(), $distro, $repo, $arch, $filename);

			$lastModified = 0;
			$contentLength = 0;

			try
			{
				// Check the mirror servers last modified date

				$request = (new Client)->request('HEAD', $url, ['timeout' => $this->getTimeout($distro), 'connect_timeout' => $this->getTimeout($distro)]);

				$lastModified = strtotime($request->getHeader('Last-Modified')[0]);
				$contentLength = (int) $request->getHeader('Content-Length')[0];
			}
			catch(GuzzleException $e)
			{
				if($e->getCode() === 404)
				{
					// Nobody cares, just give them the old file if it still exists, then.
				}
			}

			try
			{
				// Have we already downloaded this file?

				if($this->fileSystem->has($filepath))
				{
					// Yes, does the mirror server have a newer version?

					if($lastModified < filemtime($filepath))
					{
						// No, is our file somehow corrupt? (currently only checks if file is larger or smaller than it should be)

						if($contentLength !== $this->fileSystem->size($filepath))
						{
							// Yes, download it again

							return $this->downloadAndStream($url, $distro, $filepath, $filename);
						}

						// File not corrupt, return what we already got.

						return $this->streamFile($filepath, $filename);
					}
					else
					{
						// Yes, download and stream it.

						return $this->downloadAndStream($url, $distro, $filepath, $filename);
					}
				}
				else
				{
					// No, download and stream it.

					return $this->downloadAndStream($url, $distro, $filepath, $filename);
				}

			}
			catch(GuzzleException $e)
			{
				if($e->getCode() === 404)
				{
					throw new NotFoundException;
				}

				// Let's try the next mirror?

				continue;
			}
		}

		throw new HttpException(500);
	}

	/**
	 * List route.
	 *
	 * @param  \mako\view\ViewFactory $view   View factory
	 * @param  string                 $distro
	 * @param  string                 $repo
	 * @param  string                 $arch
	 * @return string
	 */
	public function list(ViewFactory $view, $distro, $repo, $arch): string
	{
		$path = sprintf($this->config->get('mirrors.' . $distro . '.url'), $distro, $repo, $arch, '');

		$prefix = sprintf('%s_%s_%s_', $distro, $repo, $arch);

		$base = sprintf('%s/storage/packages/%s_%s_%s', $this->app->getPath(), $distro, $repo, $arch);

		$files = [];

		$glob = $this->fileSystem->glob($base . '*');

		if($glob)
		{
			foreach($glob as $file)
			{
				$time = filemtime($file);

				array_push($files, (object) [
					'icon' => 'file',
					'name' => str_replace($prefix, '', basename($file)),
					'modified' => date('Y-m-d H:s e', $time ? $time : 0),
					'size' => $this->humanizer->fileSize($this->fileSystem->size($file)),
				]);
			}
		}

		if(empty($files))
		{
			throw new NotFoundException;
		}

		return $view->render('list',
		[
			'path' => $path,
			'files' => $files,
		]);
	}
}
