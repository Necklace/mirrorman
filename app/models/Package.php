<?php

namespace app\models;

use mako\database\midgard\ORM;

/**
 * Package model.
 */
class Package extends ORM
{
	/**
	 * {@inheritdoc}
	 */
	protected $tableName = 'packages';
}
