<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="{{ $__charset__ }}">
		<title>Index of {{$path}} | Mirrorman</title>
		<link rel="stylesheet" href="{{$url->base()}}/assets/css/style.css">
	</head>
	<body class="index">
		<h1>Index of {{$path}}</h1>
		<table align="left">
			<thead>
				<tr>
					<th></th>
					<th>Filename</th>
					<th>Last modified</th>
					<th>Size</th>
				</tr>
				<tr>
				<th colspan="5"><hr></th>
				</tr>
			</thead>
			<tbody>
				{% foreach($files as $file) %}
					<tr>
						<td><img src="{{$url->base()}}/assets/img/{{$file->icon}}.svg"></td>
						<td><a href="{{$file->name}}">{{$file->name}}</a></td>
						<td>{{$file->modified}}</td>
						<td>{{$file->size}}</td>
					</tr>
				{% endforeach %}
			</tbody>
		</table>
	</body>
</html>
