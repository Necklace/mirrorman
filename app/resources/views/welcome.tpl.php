<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="{{ $__charset__ }}">
		<title>Mirrorman</title>
		<link rel="stylesheet" href="{{$url->base()}}/assets/css/style.css">
	</head>
	<body>
		<div class="welcome">
			<span class="mako"><a title="Head over to the documentation" href="https://bitbucket.com/necklace/mirrorman">Mirrorman</a></span>
			{% if($saveable) %}
				<span>Currently mirroring: {{$size}} ({{$saveable}} saveable)</span>
			{% else %}
				<span>Currently mirroring: {{$size}}</span>
			{% endif %}
		</div>
	</body>
</html>

