<?php

use mako\http\routing\Routes;

/** @var \mako\http\routing\Routes $routes */

$routes->group(['namespace' => 'app\controllers'], function(Routes $routes): void
{
	$routes->get('/', 'Mirror::welcome');

	// Arch:

	$routes->get('/{distro}/{repo}/os/{arch}/{filename}', 'Mirror::get');
	$routes->get('/{distro}/{repo}/os/{arch}/', 'Mirror::list');

	// Manjaro / Msys:

	$routes->get('/{distro}/{repo}/{arch}/{filename}', 'Mirror::get');
	$routes->get('/{distro}/{repo}/{arch}/', 'Mirror::list');
});
